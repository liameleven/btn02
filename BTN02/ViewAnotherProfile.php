<?php 
ob_start();
  require_once 'init.php';
  if(!$currentUser)
  {
    header('Location: index.php');
    exit();
  }

?>
<?php include 'header.php'; ?>

<h1>Tìm Bạn Bốn Phương</h1>

<?php foreach(loadUser() as $user):
?>
  <?php $userID=$user['ID']; ?>
  <?php $profile=findUserByID($user['ID']); ?>
  <?php $isfollowing=getFriendShip($currentUser['ID'],$userID); ?>
  <?php $isfollower=getFriendShip($userID,$currentUser['ID']); ?>
      <div style="padding: 20px;overflow:auto;border:2px solid;margin:5px;">
        <img style="float:left" src="getImage.php?type=avatar&id=<?php echo $user['ID']?>" width="42" height="42">
        <span><?php echo $user['Name']?></span>
        <?php if($currentUser['ID']==$userID): ?>
        				<FORM  action="profile.php">                          
                          <button type="submit" class="btn btn-primary">Trang cá nhân</button>  
                        </FORM>
        <?php else: ?>          
        	<?php if($isfollowing && $isfollower): ?>
                        <span class="badge badge-primary">Bạn Bè</span>  
                        <FORM method="POST" action="remove-friend.php">
                          <input type="hidden" name="id" value="<?php echo $userID; ?>">
                          <button type="submit" class="btn btn-primary">Hủy Kết Bạn</button>  
                        </FORM>
                    <?php else: ?>
                      <?php if ($isfollowing && !$isfollower): ?>
                      
                        <FORM method="POST" action="remove-friend-request.php">
                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                            <button type="submit" class="btn btn-primary">Hủy Yêu Cầu Kết Bạn</button>  
                        </FORM>
                      <?php endif; ?>
                      <?php if(!$isfollowing && $isfollower): ?>
                      	<br>
                     	<div class="btn-group" role="group" aria-label="Basic example">                     		
                        <FORM method="POST" action="add-friend.php">
                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                            <button type="submit" class="btn btn-primary" class="float:right">Đồng Ý Yêu Cầu Kết Bạn</button>  
                        </FORM>
                        <FORM method="POST" action="refuse-friend-request.php">
                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                            <button type="submit" class="btn btn-primary" class="float:right">Từ Chối Yêu Cầu Kết Bạn</button>  
                        </FORM>
                    	</div>
                      <?php endif; ?>
                      <?php if(!$isfollowing && !$isfollower): ?>
                        <FORM method="POST" action="add-friend.php">
                            <input type="hidden" name="id" value="<?php echo $userID; ?>">
                            <button type="submit" class="btn btn-primary"> Kết Bạn</button>  
                        </FORM>
                      <?php endif; ?>
                  <?php endif; ?>
             <?php endif; ?>
      </div>
<?php
endforeach; ?>
<?php include 'footer.php'; ?>