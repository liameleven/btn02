<?php 
    require_once 'init.php';
    include 'header.php';
?>
<?php if(isset($_POST['Name']) && isset($_POST['Email']) && ($_POST['Pass'])): ?>
<?php 
    $name=$_POST['Name'];
    $email=$_POST['Email'];
    $pass=$_POST['Pass'];
    $success=false; 
    $user=findUserByEmail($email);
    if(!$user){
        InsertUser($name,$email,$pass);
        $success=true;
    }
?>
<?php if($success): ?>
<div class="alert alert-success" role="alert">
    Đăng ký thành công
</div>
<?php else: ?>
<div class="alert alert-primary" role="alert">
    Đăng ký thất bại!!!
</div>
<?php endif; ?>
<?php else: ?>
<h1>Đăng kí</h1>
<form method="POST" action="register.php">
  <div  class="form-group">
    <label ><strong>Name</strong></label>
    <input type="text" class="form-control" name="Name" id="name" placeholder="Nhập name ...">
  </div>
  <div  class="form-group">
    <label ><strong>Email</strong></label>
    <input type="email" class="form-control" name="Email" id="email" placeholder="Nhập Email ...">
  </div>
  <div class="form-group">
    <label ></label><strong>Mật khẩu</strong></label>
    <input type="password" class="form-control" name="Pass" id="pass" placeholder="Nhập mật khẩu ...">
  </div>
  <button  type="submit" class="btn btn-primary">Đăng ký </button>
</form>
<?php endif; ?>
<?php include 'footer.php'; ?>