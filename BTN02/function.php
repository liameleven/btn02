<?php
require_once("config.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
$pdo = new PDO($DB_TYPE.'='.$DB_HOST.';'.$DB_NAME_TYPE.'='.$DB_NAME.$DB_OPTIONAL,$DB_USER,$DB_PASSWORD);

function generateRandomString($length = 16) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function InsertUser($name,$email,$pass){    
    global $pdo;
    global $BASE_URL;
    $hashPassword = password_hash($pass, PASSWORD_DEFAULT);
    $code = generateRandomString(16);
    $stmt = $pdo->prepare("INSERT INTO user (Name, Email, Pass, Status, Code, CodeForgot) VALUES (?, ?, ?, ?, ?, ?)");
    $stmt->execute(array($name, $email, $hashPassword, 0, $code,0));
    $id = $pdo->lastInsertId();
    sendEmail($email, $name, 'Kích hoạt tài khoản', "Mã kích hoạt tài khoản của bạn là <a href=\"$BASE_URL/activate.php?code=$code\">$BASE_URL/activate.php?code=$code</a>");
    return $id;
    
}

function findUserByEmail($email){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM user WHERE Email=?");
    $stmt->execute(array($email));
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function findUserByID($userid){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM user WHERE ID=?");
    $stmt->execute(array($userid));
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function userPost($user,$content,$image)
{
    global $pdo;   
    $stmt = $pdo->prepare("INSERT post(Content,UserID,IMGContent) values (?,?,?)");
    $stmt->execute(array($content,$user['ID'], $image) );
    return $pdo->lastInsertId();
}
function loadPost()
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT p.Content,p.Time,p.ID,u.Name,u.ID uid FROM post p JOIN user u ON u.ID=p.UserID ORDER BY p.Time DESC ");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function getImageUser($id)
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT Image FROM user WHERE ID=?");
    $stmt->execute(array($id));
    $image = $stmt->fetch(PDO::FETCH_ASSOC);
    return $image['Image'];
}
function checkImageType($type)
{
    $allowed = array("image/jpeg", "image/gif", "image/png");
    if(!in_array($type, $allowed))
        return false;
    return true;
}
function getImagePost($id)
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT IMGContent FROM post WHERE ID=?");
    $stmt->execute(array($id));
    $image = $stmt->fetch(PDO::FETCH_ASSOC);
    return $image['IMGContent'];
}



function checkEmail($Email)
{
    global $pdo; 
    $stmt = $pdo->prepare("SELECT * FROM user WHERE Email=?");
    $stmt->execute(array($Email));
    $tmp=$stmt->fetch(PDO::FETCH_ASSOC);
    if($tmp!=null)
    {
        return true;
    }
    else
    {
        return false;
    }
}

function SendMailFogetPasss($name,$email)
    {
        global $pdo,$BASE_URL;
        $code=generateRandomString(16);
        $stmt = $pdo->prepare("UPDATE user SET CodeForgot=? where Email=?");
        $stmt->execute(array($code,$email));
        $pdo->lastInsertId();
        sendEmail($email,$name,'Thiết lập lại mật khẩu',"Mã kích hoạt tài khoản của bạn là <a href=\"$BASE_URL/checkCodeForgot.php?code=$code\">$BASE_URL/checkCodeForgot.php?code=$code</a>");
    }

function UpdatePass($email,$pass)
    {
        global $pdo;
        $haspass=password_hash($pass,PASSWORD_DEFAULT);
        $stmt = $pdo->prepare("UPDATE user SET Pass=? where Email=?");
        $stmt->execute(array($haspass,$email));
        $pdo->lastInsertId();
    }

function ChangePass($pass,$id)
    {
        global $pdo;
        $haspass=password_hash($pass,PASSWORD_DEFAULT);
        $stmt = $pdo->prepare("UPDATE user SET Pass=? where ID=?");
        return $stmt->execute(array($haspass,$id));
    }

function activateUser($code) {
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM user WHERE Code = ? AND Status = ?");
    $stmt->execute(array($code, 0));
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($user && $user['Code'] == $code) {
      $stmt = $pdo->prepare("UPDATE user SET Code = ?, Status = ? WHERE ID = ?");
      $stmt->execute(array('', 1, $user['ID']));
      return true;
    }
    return false;
}
function updateProfile($name,$phone,$id)
{
    global $pdo;
    $stmt = $pdo->prepare("UPDATE user SET Name= ?,PhoneNumber=? where ID=?");
    return $stmt->execute(array($name,$phone,$id));
}
function upIMGtoSql($img,$id)
{
    global $pdo;
    $stmt = $pdo->prepare("UPDATE user SET Image=? WHERE ID=?");
    $stmt->execute(array($img,$id));
    $pdo->lastInsertId();
}
function uploadFileToSql($temp):void
{
    $image=file_get_contents($_FILES['file']['tmp_name']);
    upIMGtoSql($image,$temp);
}

function updateAddress($temp,$id)
{
    global $pdo;
    $stmt = $pdo->prepare("UPDATE user SET Address=? where ID=?");
    $stmt->execute(array($temp,$id));
    $pdo->lastInsertId();
}
function updatePhoneNumber($temp,$id)
{
    global $pdo;
    $stmt = $pdo->prepare("UPDATE user SET PhoneNumber=? where ID=?");
    $stmt->execute(array($temp,$id));
    $pdo->lastInsertId();
}
function updateAboutMe($temp,$id)
{
    global $pdo;
    $stmt = $pdo->prepare("UPDATE user SET AboutMe=? where ID=?");
    $stmt->execute(array($temp,$id));
    $pdo->lastInsertId();
}
function getCoverImage($id)
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT CoverImage FROM user WHERE ID=?");
    $stmt->execute(array($id));
    $image = $stmt->fetch(PDO::FETCH_ASSOC);
    return $image['CoverImage'];
}
function saveCoverImage($id)
{
    $image=file_get_contents($_FILES['fileAvatar']['tmp_name']);
    global $pdo;
    $stmt = $pdo->prepare("UPDATE user SET CoverImage=? WHERE ID=?");
    $stmt->execute(array($image,$id));
    $pdo->lastInsertId();
}
function GetStatusByUserID($id)
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM post where UserID=?  ORDER BY Time DESC");
    $stmt->execute(array($id));
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function sendEmail($to, $name, $subject, $content) {
    global $EMAIL_FROM, $EMAIL_NAME, $EMAIL_PASSWORD;
    $mail = new PHPMailer(true);
  
    //Server settings
	$mail->SMTPDebug = SMTP::DEBUG_SERVER;  
    $mail->isSMTP();                                           
    $mail->CharSet    = 'UTF-8';
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = $EMAIL_FROM;                     // SMTP username
    $mail->Password   = $EMAIL_PASSWORD;                               // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
    $mail->Port       = 587;                                    // TCP port to connect to
  
    //Recipients
    $mail->setFrom($EMAIL_FROM, $EMAIL_NAME);
    $mail->addAddress($to, $name);    
    $mail->Subject = $subject;
    $mail->Body    = $content;
    $mail->send();
}
function sendFriendRequest($userId1,$userId2)
{
    global $pdo;
    $stmt=$pdo->prepare("INSERT INTO friendship(userID1,userID2) VALUES(?,?)");
    $stmt->execute(array($userId1,$userId2));
    $pdo->lastInsertId();
}
function getFriendShip($userId1,$userId2)
{
    global $pdo;
    $stmt=$pdo->prepare("SELECT * FROM friendship WHERE userID1=? AND userID2=? ");
    $stmt->execute(array($userId1, $userId2));
    return $stmt->fetch(PDO::FETCH_ASSOC);
}
function removeFriendShip($userId1,$userId2)
{
    global $pdo;
    $stmt=$pdo->prepare("DELETE FROM friendship WHERE userID1=? AND userID2=? ");
    $stmt->execute(array($userId1,$userId2));  
    $pdo->lastInsertId(); 
}
function loadUser()
{
    global $pdo;
    $stmt = $pdo->prepare("SELECT u.ID,u.Name,u.Image,u.CoverImage uid FROM  user u  ORDER BY u.ID DESC ");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
