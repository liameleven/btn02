<?php
session_start();
require_once('./vendor/autoload.php');
require_once('config.php');
require_once('function.php');
$DEBUGGER = 1;
if ($DEBUGGER)
{
    require_once './vendor/autoload.php';
    require_once 'function.php';

    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(E_ALL);
}
$CurrentPage = basename($_SERVER['PHP_SELF'],".php");
date_default_timezone_set('Asia/Ho_Chi_Minh');
$currentUser=null;
if(isset($_SESSION['userid'])){
    $currentUser=findUserByID($_SESSION['userid']);
}

