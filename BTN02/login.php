<?php 
    require_once 'init.php';
    include 'header.php';
?>
<?php if(isset($_POST['Email']) && ($_POST['pass'])): ?>
<?php 
    $email=$_POST['Email'];
    $pass=$_POST['pass'];
    $success=false;
    $user=findUserByEmail($email);
    if($user && password_verify($pass, $user['Pass'])){
        $_SESSION['userid']=$user['ID'];
        $success=true;
    }
?>
<?php if($success): ?>
<?php header('Location: index.php'); ?>
<?php else: ?>
    <div class="alert alert-primary" role="alert">
    <h1>Đăng nhập thất bại!!!</h1>
    </div>
<?php endif; ?>
<?php else: ?>
<h1>Đăng nhập</h1>
    <form method="POST" action="login.php">
        <div  class="form-group">
            <label ><strong>Tài khoản</strong></label>
            <input type="email" class="form-control" name="Email" id="Email" placeholder="Nhập tài khoản ...">
        </div>
        <div class="form-group">
            <label ></label><strong>Mật khẩu</strong></label>
            <input type="password" class="form-control" name="pass" id="pass" placeholder="Nhập mật khẩu ...">
            <br>
            <a href="forgotPassword.php" class="lost">Quên mật khẩu?</a>
        </div>
        <button  type="submit" class="btn btn-primary">Đăng nhập</button>
    </form>
<?php endif; ?>
<?php include 'footer.php'; ?>