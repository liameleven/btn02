<?php
require_once("init.php");
require_once("function.php");
include "header.php";
?>
<?php
if(empty($currentUser))
{
    ?>
    BAN CAN DANG NHAP DE SU DUNG DICH VU...
    <?php
    die();
}

$error = 0;
if (isset($_POST['submit']) )
{
    if (!empty($_POST['content']))
    {
        $content = $_POST['content'];
        $hvimage = true;
        if (!empty($_FILES['file'])){
            $fileupload = $_FILES['file'];
            if (!$fileupload["error"])
            {
                if (!checkImageType($fileupload['type']))
                    $error =1;
                else if ($fileupload['size'] > 30*1024)
                    $error =2;
            }
            else $hvimage = false;
        }
        $image = "";
        
        if (!$error){       
            $user = $currentUser;            
            if (isset($hvimage))
                $image = file_get_contents($fileupload['tmp_name']);
            userPost($user,$content,$image);
        }
    } else 
        $error = -1;
} else 
    $error = -3;
?>
<div>
  <div style="margin:5px;">
    <h1>Đăng bài viết</h1>
<?php
if (!$error):
?>
    <h2 style="color:chartreuse;">Dang tin thanh cong</h2>
<?php
elseif ($error == 1) :
?>
    <h2 style="color:red;">Dinh dang files khong chinh xac</h2>
<?php
elseif ($error == 2) :
?>
    <h2 style="color:red;">Kich thuoc files vuot qua muc quy dinh</h2>
<?php
elseif ($error == -1):
?>
    <h2 style="color:red;">Ban phai nhap du lieu de dang bai</h2>
<?php
endif;
?>    
    <form action="index.php" method="post" enctype="multipart/form-data">
      <textarea name="content" rows="4" cols="50" placeholder="<?php echo $currentUser['Name']?> ơi, bạn đang nghĩ gì?"></textarea>
      <br>
      <br>
      <input type="file" name="file">
      <br>
      <p><strong>Ghi chú:</strong> Chỉ cho phép định dạng files: .jpg, .jpeg, .gif và kích thước tối đa tệp tin là 30kb.</p>
      <br>
      <input type="submit" name="submit" value="Đăng bài viết">
    </form>
  </div>
  
  <hr>
  
  <div>
    <h2>Danh sách bài viết</h2>
    <div>
<?php
foreach(loadPost() as $post):
?>
      <div style="padding: 20px;overflow:auto;border:2px solid;margin:5px;">
        <img style="float:left" src="getImage.php?type=avatar&id=<?php echo $post['uid']?>" width="42" height="42">
        <span><?php echo $post['Name']?></span><br>
        <span><?php echo $post['Time']?></span>
        <pre><?php echo $post['Content']?>
        </pre>
        <img style="max-width: 500px;max-height: 200px;" src="getImage.php?type=post&id=<?php echo $post['ID']?>">
      </div>
<?php
endforeach;
?>
    </div>
  </div>
</div>

<?php          
include "footer.php"; 